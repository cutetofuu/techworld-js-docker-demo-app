# commands

## create docker network

```
docker network create mongo-network
```

## start mongodb

```
docker run -d \
-p 27018:27017 \
-e MONGO_INITDB_ROOT_USERNAME=admin \
-e MONGO_INITDB_ROOT_PASSWORD=password \
--net mongo-network \
--name mongodb-2 \
mongo
```

## start mongo-express

```
docker run -d \
-p 8081:8081 \
-e ME_CONFIG_MONGODB_ADMINUSERNAME=admin \
-e ME_CONFIG_MONGODB_ADMINPASSWORD=password \
-e ME_CONFIG_MONGODB_SERVER=mongodb-2 \
--net mongo-network \
--name mongo-express-2 \
mongo-express
```
